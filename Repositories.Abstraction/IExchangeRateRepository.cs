﻿using PoC.Repositories.Abstraction.Entities;
using System;
using System.Threading.Tasks;

namespace PoC.Repositories.Abstraction
{
    public interface IExchangeRateRepository
    {
        Task<ExchangeRate> GetAsync(DateTime date, Currency from, Currency to);
    }
}
