﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PoC.Repositories.Abstraction.Entities
{
    public enum Currency
    {
        PLN,
        EUR,
        USD
    }
}
