﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PoC.Repositories.Abstraction.Entities
{
    public class ExchangeRate
    {
        public int Id { get; set; }

        public decimal Rate { get; set; }

        public bool RoundUpFrom51 { get; set; }

        public Currency From { get; set; }

        public Currency To { get; set; }

        public DateTime Date { get; set; }
    }
}
