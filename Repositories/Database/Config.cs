﻿using PoC.Repositories;
using PoC.Repositories.Database;
using PoC.Repositories.Abstraction;
using Microsoft.EntityFrameworkCore;


namespace PoC.Repositories.Database
{
    public static class Config
    {
        public static DbContextOptionsBuilder ConfigureExchangeRateDb (this DbContextOptionsBuilder builder, string connectionString)
        {
            return builder.UseInMemoryDatabase("ExchangeRateDb");
        }

    }
}
