﻿using Microsoft.EntityFrameworkCore;
using PoC.Repositories.Abstraction.Entities;

namespace PoC.Repositories.Database
{
    public class ExchangeRateDb : DbContext
    {
        public ExchangeRateDb(DbContextOptions<ExchangeRateDb> options) : base(options)
        {

        }

        public virtual DbSet<ExchangeRate> ExchangeRates { get; set; }

    }
}
