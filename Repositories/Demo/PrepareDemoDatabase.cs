﻿using PoC.Repositories.Database;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using PoC.Repositories.Abstraction.Entities;

namespace PoC.Repositories.Demo
{
    public static class PrepareDemoDatabase
    {
        public static void Populate(ExchangeRateDb db)
        {
            if (!db.ExchangeRates.Any())
            {
                var entites = Enumerable.Range(0, 4*365).SelectMany(x =>
                new ExchangeRate[]
                {
                    new ExchangeRate
                    {
                        Date = DateTime.Today.AddDays(-x),
                        From = Currency.EUR,
                        To = Currency.PLN,
                        Rate = 4.4444M,
                        RoundUpFrom51 = x < 8
                    },
                    new ExchangeRate
                    {
                        Date = DateTime.Today.AddDays(-x),
                        From = Currency.PLN,
                        To = Currency.EUR,
                        Rate = 0.2252M,
                        RoundUpFrom51 = x < 8
                    },
                    new ExchangeRate
                    {
                        Date = DateTime.Today.AddDays(-x),
                        From = Currency.USD,
                        To = Currency.PLN,
                        Rate = 4.0001M,
                        RoundUpFrom51 = x < 8
                    },
                    new ExchangeRate
                    {
                        Date = DateTime.Today.AddDays(-x),
                        From = Currency.PLN,
                        To = Currency.USD,
                        Rate = 0.2501M,
                        RoundUpFrom51 = x < 8
                    },
                    new ExchangeRate
                    {
                        Date = DateTime.Today.AddDays(-x),
                        From = Currency.USD,
                        To = Currency.EUR,
                        Rate = 1.1111M,
                        RoundUpFrom51 = x < 8
                    },
                    new ExchangeRate
                    {
                        Date = DateTime.Today.AddDays(-x),
                        From = Currency.EUR,
                        To = Currency.USD,
                        Rate = 1.1111M,
                        RoundUpFrom51 = x < 8
                    }
                });

                db.ExchangeRates.AddRange(entites);
                db.SaveChanges();
            }
        }


    }
}
