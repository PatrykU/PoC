﻿using Microsoft.EntityFrameworkCore;
using PoC.Repositories.Abstraction;
using PoC.Repositories.Abstraction.Entities;
using PoC.Repositories.Database;
using PoC.Repositories.Exceptions;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PoC.Repositories
{
    public class ExchangeRateRepository : IExchangeRateRepository
    {
        private readonly ExchangeRateDb db;

        public ExchangeRateRepository(ExchangeRateDb db)
        {
            this.db = db;
        }

        public async Task<ExchangeRate> GetAsync(DateTime date, Currency from, Currency to)
        {
            var entity = await db.ExchangeRates
                .Where(x => x.Date.Date.Equals(date.Date))
                .Where(x => x.From == from)
                .Where(x => x.To == to)
                .SingleOrDefaultAsync();//For discusion, Why not FirstOrDefault ?

            if(entity == null)
            {
                throw new NotFoundException();
            }

            return entity;
        }
    }
}
