﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PoC.ApplicationServicesProvider;
using PoC.Services.Abstraction;
using PoC.Services.Validators;
using PoC.Services.Abstraction.Dto;
using FluentValidation;

namespace OldProject.Controllers
{
    public class CurrencyExchangeController : Controller
    {
        private readonly ICurrencyExchangeService CurrencyExchangeService;
        private readonly IValidator<CurrencyExchangeRequest> Validator;

        public CurrencyExchangeController()
        {
            Validator = ServicesProvider.Get<IValidator<CurrencyExchangeRequest>>();
            CurrencyExchangeService = ServicesProvider.Get<ICurrencyExchangeService>();
        }

        public ActionResult Index()
        {
            AddCurrencySelectList();
            return View();
        }

        [HttpPost]
        public ActionResult Exchange(CurrencyExchangeRequest request)
        {
            var validation = Validator.Validate(request);
            if (validation.IsValid)
            {
                var model = CurrencyExchangeService.ExchangeCurrencyAsync(request).Result;
                return View(model);
            }
            validation.Errors.ForEach(x => ModelState.AddModelError(x.PropertyName, x.ErrorMessage));
            AddCurrencySelectList();
            return View("Index", request);
        }

        private void AddCurrencySelectList()
        {
            IEnumerable<SelectListItem> currencySelectList = Enum.GetValues(typeof(Currency)).Cast<Currency>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString()
            });

            ViewData["currencySelectList"] = currencySelectList;
        }
    }
}