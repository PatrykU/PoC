﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PoC.ApplicationServicesProvider;
using PoC.Services.Abstraction;
using PoC.Services.Validators;
using PoC.Services.Abstraction.Dto;
using FluentValidation;

namespace OldProject.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICurrencyExchangeService currencyExchangeService;
        private readonly IValidator<CurrencyExchangeRequest> CurrencyExchangeRequestValidator;

        public HomeController()
        {
            CurrencyExchangeRequestValidator = ServicesProvider.Get<IValidator<CurrencyExchangeRequest>>();
            currencyExchangeService = ServicesProvider.Get<ICurrencyExchangeService>();
        }

        public ActionResult Index()
        {
            return View();
        }


    }
}