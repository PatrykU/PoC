﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using PoC.ApplicationServicesProvider;
using PoC.Repositories.Database;
using PoC.Repositories.Demo;
using System.Threading.Tasks;

namespace OldProject
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            Task.Run(PopulateDatabase).Wait();

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected static void PopulateDatabase()
        {
            var db = ServicesProvider.Get<ExchangeRateDb>();
            PrepareDemoDatabase.Populate(db);
        }
    }
}
