﻿using Ninject.Modules;
using PoC.Repositories;
using PoC.Repositories.Database;
using PoC.Repositories.Abstraction;
using Microsoft.EntityFrameworkCore;

namespace PoC.Repositories.DependencyInjection.Ninject
{
    public class Module : NinjectModule
    {
        private readonly string connectionString;

        public Module(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public override void Load()
        {
            Bind<IExchangeRateRepository>().To<ExchangeRateRepository>().InThreadScope();
            Bind<ExchangeRateDb>().ToSelf().InThreadScope().WithConstructorArgument<DbContextOptions<ExchangeRateDb>>(CreateExchangeRateDbContextOptions());
        }

        protected DbContextOptions<ExchangeRateDb> CreateExchangeRateDbContextOptions()
        {
            DbContextOptionsBuilder<ExchangeRateDb> builder = new DbContextOptionsBuilder<ExchangeRateDb>();
            builder.ConfigureExchangeRateDb(connectionString);
            return builder.Options;
        }
    }
}
