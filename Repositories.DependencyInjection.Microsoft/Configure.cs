﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PoC.Repositories.Abstraction;
using PoC.Repositories.Database;

namespace PoC.Repositories.DependencyInjection.Microsoft
{
    public static class Configure
    {
        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddScoped<IExchangeRateRepository, ExchangeRateRepository>();
            return services;
        }

        public static IServiceCollection AddEntityFramework(this IServiceCollection services, IConfiguration configuration)
        {

            string connectionString = string.Empty;//normaly use configuration.GetConnectionString("ExchangeRate");
            services.AddDbContext<ExchangeRateDb>(opt => opt.ConfigureExchangeRateDb(connectionString));
            return services;
        }
    }
}
