﻿using PoC.Repositories.DependencyInjection.Ninject;
using PoC.Services.DependencyInjection.Ninject;
using Ninject;

namespace PoC.ApplicationServicesProvider
{
    public static class ServicesProvider
    {
        static ServicesProvider()
        {
            Kernel = new StandardKernel(new PoC.Services.DependencyInjection.Ninject.Module(),
                new PoC.Repositories.DependencyInjection.Ninject.Module(string.Empty));
        }

        public static IKernel Kernel { get; private set; }

        public static T Get<T>()
        {
            return Kernel.Get<T>();
        }
    }
}
