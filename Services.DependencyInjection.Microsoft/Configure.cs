﻿using FluentValidation.AspNetCore;
using Microsoft.Extensions.DependencyInjection;
using PoC.Services.Abstraction;
using PoC.Services.Startup;
using System;

namespace PoC.Services.DependencyInjection.Microsoft
{
    public static class Configure
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddAutoMapper(cfg => cfg.ConfigureMapper());

            services.AddScoped<ICurrencyExchangeService, CurrencyExchangeService>();
            return services;
        }

        public static IMvcBuilder AddValidation(this IMvcBuilder mvcBuilder, Action<FluentValidationMvcConfiguration> configure = null)
        {
            mvcBuilder.AddFluentValidation(fv =>
            {
                fv.RegisterValidatorsFromAssemblyContaining<PoC.Services.Validators.CurrencyExchangeValidator>();
                if (configure != null)
                {
                    configure(fv);
                }
            });
            return mvcBuilder;
        }
    }
}
