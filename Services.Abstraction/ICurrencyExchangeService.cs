﻿using PoC.Services.Abstraction.Dto;
using System.Threading.Tasks;

namespace PoC.Services.Abstraction
{
    public interface ICurrencyExchangeService
    {
        Task<CurrencyExchangeResult> ExchangeCurrencyAsync(CurrencyExchangeRequest dto);
    }
}
