﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PoC.Services.Abstraction.Dto
{
    public class CurrencyExchangeResult
    {
        public Currency FromCurrency { get; set; }

        public decimal Amount { get; set; }

        public DateTime Date { get; set; }

        public Currency ToCurrency { get; set; }

        public decimal ExchangeResult { get; set; }
    }
}
