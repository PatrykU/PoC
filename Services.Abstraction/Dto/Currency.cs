﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PoC.Services.Abstraction.Dto
{
    public enum Currency
    {
        PLN = 1,
        EUR,
        USD
    }
}