﻿using System;
using System.Collections.Generic;
using System.Text;
using PoC.Services.Abstraction;
using PoC.Services.Abstraction.Dto;
using PoC.Repositories.Abstraction;
using Entities = PoC.Repositories.Abstraction.Entities;
using AutoMapper;
using System.Threading.Tasks;

namespace PoC.Services
{
    public class CurrencyExchangeService : ICurrencyExchangeService
    {
        private readonly IMapper mapper;
        private readonly IExchangeRateRepository exchangeRateRepository;


        public CurrencyExchangeService(IMapper mapper, IExchangeRateRepository exchangeRateRepository)
        {
            this.exchangeRateRepository = exchangeRateRepository;
            this.mapper = mapper;
        }

        public async Task<CurrencyExchangeResult> ExchangeCurrencyAsync(CurrencyExchangeRequest dto)
        {
            var exchangeRate = await exchangeRateRepository.GetAsync(dto.Date, mapper.Map<Entities.Currency>(dto.FromCurrency), mapper.Map<Entities.Currency>(dto.ToCurrency));

            decimal exchangedValue = dto.Amount * exchangeRate.Rate;
            exchangedValue = RoundUp(exchangedValue, exchangeRate.RoundUpFrom51);

            var result = mapper.Map<CurrencyExchangeResult>(dto);
            result.ExchangeResult = exchangedValue;
            return result;
        }

        private static decimal RoundUp(decimal exchangedValue, bool roundUpFrom51)
        {
            if (roundUpFrom51)
            {
                exchangedValue *= 10000;
                int exchangeIntigerValue = (int)exchangedValue;
                exchangeIntigerValue += 49;
                exchangeIntigerValue /= 100;
                exchangedValue = exchangeIntigerValue / 100M;
            }
            else
            {
                exchangedValue *= 1000;
                int exchangeIntigerValue = (int)exchangedValue;
                exchangeIntigerValue += 5;
                exchangeIntigerValue /= 10;
                exchangedValue = exchangeIntigerValue / 100M;
            }

            return exchangedValue;
        }
    }
}
