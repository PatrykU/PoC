﻿using AutoMapper;
using Dto = PoC.Services.Abstraction.Dto;
using Entity = PoC.Repositories.Abstraction.Entities;


namespace PoC.Services.Startup
{
    public static class MapperConfigurationExtensions
    {
        public static IMapperConfigurationExpression ConfigureMapper(this IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<Dto.CurrencyExchangeRequest, Dto.CurrencyExchangeResult>()
                .ForMember(dest => dest.ExchangeResult, opt => opt.Ignore());

            cfg.CreateMap<Dto.Currency, Entity.Currency>()
                .ReverseMap();

            return cfg;
        }

    }
}
