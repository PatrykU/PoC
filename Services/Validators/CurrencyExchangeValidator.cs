﻿using FluentValidation;
using PoC.Services.Abstraction.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace PoC.Services.Validators
{
    public class CurrencyExchangeValidator : AbstractValidator<CurrencyExchangeRequest>
    {
        public CurrencyExchangeValidator()
        {
            RuleFor(x => x.Amount).NotEmpty().ScalePrecision(2, 18);
            RuleFor(x => x.Date).NotEmpty();
            RuleFor(x => x.FromCurrency).NotEmpty().IsInEnum().NotEqual(x => x.ToCurrency);
            RuleFor(x => x.ToCurrency).NotEmpty().IsInEnum().NotEqual(x => x.FromCurrency);
        }
    }
}
