﻿using NUnit.Framework;
using NSubstitute;
using System;
using System.Collections.Generic;
using PoC.Services;
using PoC.Repositories.Abstraction;
using System.Threading.Tasks;
using PoC.Repositories.Abstraction.Entities;
using PoC.Services.Startup;
using AutoMapper;
using Dto = PoC.Services.Abstraction.Dto;
using Entity = PoC.Repositories.Abstraction.Entities;

namespace Services.Tests
{

    public class CurrencyExchangeServiceShould
    {
        [TestCase(4.0049, true, 4.00)]
        [TestCase(4.0049, false, 4.00)]
        [TestCase(4.0050, true, 4.00)]
        [TestCase(4.0050, false, 4.01)]
        [TestCase(4.0051, true, 4.01)]
        [TestCase(4.0051, false, 4.01)]
        public async Task ExchangeCurrencyWithRoundUpRule(decimal rate, bool roundAt51, decimal expectedValue)
        {
            //Arrange
            var repository = Substitute.For<IExchangeRateRepository>();
            repository.GetAsync(default, default, default)
                .ReturnsForAnyArgs<ExchangeRate>(x => new ExchangeRate
                {
                    Id = 7,
                    From = x.ArgAt<Currency>(1),
                    To = x.ArgAt<Currency>(2),
                    Date = x.Arg<DateTime>().Date,
                    Rate = rate,
                    RoundUpFrom51 = roundAt51
                });

            var mapper = Substitute.For<IMapper>();
            mapper.Map<Currency>(Arg.Is(Dto.Currency.EUR)).Returns(Entity.Currency.EUR);
            mapper.Map<Currency>(Arg.Is(Dto.Currency.PLN)).Returns(Entity.Currency.PLN);
            mapper.Map<Dto.CurrencyExchangeResult>(Arg.Any<Dto.CurrencyExchangeRequest>()).Returns(new Dto.CurrencyExchangeResult());

            var request = new Dto.CurrencyExchangeRequest
            {
                Date = DateTime.Today,
                Amount = 1,
                FromCurrency = Dto.Currency.EUR,
                ToCurrency = Dto.Currency.PLN
            };
            var sut = new CurrencyExchangeService(mapper, repository);

            //Act
            var result = await sut.ExchangeCurrencyAsync(request);

            //Assert
            Assert.Multiple(() =>
            {
                Assert.AreEqual(expectedValue, result.ExchangeResult);
                mapper.Received().Map<Dto.CurrencyExchangeResult>(request);
                repository.Received().GetAsync(request.Date, Currency.EUR, Currency.PLN);
            });
        }


        [TestCase(4.0049, true, 4.00)]
        [TestCase(4.0049, false, 4.00)]
        [TestCase(4.0050, true, 4.00)]
        [TestCase(4.0050, false, 4.01)]
        [TestCase(4.0051, true, 4.01)]
        [TestCase(4.0051, false, 4.01)]
        public async Task ExchangeCurrencyWithMapperWithRoundUpRule(decimal rate, bool roundAt51, decimal expectedValue)
        {
            //Arrange
            var repository = Substitute.For<IExchangeRateRepository>();
            repository.GetAsync(default, default, default)
                .ReturnsForAnyArgs<ExchangeRate>(x => new ExchangeRate
                {
                    Id = 7,
                    From = x.ArgAt<Currency>(1),
                    To = x.ArgAt<Currency>(2),
                    Date = x.Arg<DateTime>().Date,
                    Rate = rate,
                    RoundUpFrom51 = roundAt51
                });

            var mapperConfig = new AutoMapper.MapperConfiguration((Action<IMapperConfigurationExpression>)(cfg => cfg.ConfigureMapper()));
            var mapper = new Mapper(mapperConfig);

            var request = new Dto.CurrencyExchangeRequest
            {
                Date = DateTime.Today,
                Amount = 1,
                FromCurrency = Dto.Currency.EUR,
                ToCurrency = Dto.Currency.PLN
            };

            var sut = new CurrencyExchangeService(mapper, repository);

            //Act
            var result = await sut.ExchangeCurrencyAsync(request);

            //Assert
            Assert.Multiple(() =>
            {
                Assert.AreEqual(expectedValue, result.ExchangeResult);
                Assert.AreEqual(request.Date, result.Date);
                Assert.AreEqual(request.FromCurrency, result.FromCurrency);
                Assert.AreEqual(request.ToCurrency, result.ToCurrency);
                Assert.AreEqual(request.Amount, result.Amount);
                // and more Assert.AreEqual(result. , result.  );
                repository.Received().GetAsync(request.Date, Currency.EUR, Currency.PLN);
            });
        }
    }
}
