﻿using NUnit.Framework;
using FluentValidation;
using FluentValidation.TestHelper;
using PoC.Services.Validators;
using PoC.Services.Abstraction.Dto;
using System.Linq;

namespace Services.Tests
{
    [TestFixture]
    public class CurrencyExchangeValidatorShould
    {

        private CurrencyExchangeValidator sut;

        [SetUp]
        public void SetUp()
        {
            sut = new CurrencyExchangeValidator();
        }

        [Test]
        public void BeValidWhenAllFieldsPresent()
        {
            var dto = new CurrencyExchangeRequest
            {
                Amount = 100.12M,
                Date = System.DateTime.Now,
                FromCurrency = Currency.EUR,
                ToCurrency = Currency.PLN
            };

            var result = sut.TestValidate(dto);

            Assert.AreEqual(true, result.IsValid,string.Join("/n",result.Errors));
        }

        [Test]
        public void BeInvalidWhenEmptyFields()
        {
            var result = sut.TestValidate(new CurrencyExchangeRequest());

            Assert.Multiple(() =>
            {
                result.ShouldHaveValidationErrorFor(x => x.Amount);
                result.ShouldHaveValidationErrorFor(x => x.Date);
                result.ShouldHaveValidationErrorFor(x => x.FromCurrency);
                result.ShouldHaveValidationErrorFor(x => x.ToCurrency);
            });
        }

        [Test]
        public void BeInvalidWhenInAndOutCurrencyAreSame()
        {
            var dto = new CurrencyExchangeRequest
            {
                FromCurrency = Currency.PLN,
                ToCurrency = Currency.PLN
            };

            var result = sut.TestValidate(dto);

            Assert.Multiple(() =>
            {
                result.ShouldHaveValidationErrorFor(x => x.FromCurrency);
                result.ShouldHaveValidationErrorFor(x => x.ToCurrency);
            });
        }

        [Test]
        public void BeInvalidWhenUnexpectedCurrency()
        {
            var dto = new CurrencyExchangeRequest
            {
                FromCurrency = 0,
                ToCurrency = (Currency)(int.MaxValue - 1)
            };

            var result = sut.TestValidate(dto);

            Assert.Multiple(() =>
            {
                result.ShouldHaveValidationErrorFor(x => x.FromCurrency);
                result.ShouldHaveValidationErrorFor(x => x.ToCurrency);
            });
        }

        [Test, Sequential]
        public void BeValidForAmountWithMax2Decimals(
            [Values(123456, 12345, 1234, 1234, 123, 12, 1, 1.1, 1.01, 12.01, 123.01, 1234.01, 12345.01, 123456.01, 1234567.01)] decimal amount)
        {
            var dto = new CurrencyExchangeRequest
            {
                Amount = amount
            };

            var result = sut.TestValidate(dto);

            result.ShouldNotHaveValidationErrorFor(x => x.Amount);
        }

        [Test, Sequential]
        public void BeInalidForAmountWithMoreThan2Decimals(
            [Values(1.001, 1.0012, 1.00123, 1.001234, 1.0012345, 1.00123456, 1.010234567, 1.0012345678)] decimal amount)
        {
            var dto = new CurrencyExchangeRequest
            {
                Amount = amount
            };

            var result = sut.TestValidate(dto);

            result.ShouldHaveValidationErrorFor(x => x.Amount);
        }
    }
}
