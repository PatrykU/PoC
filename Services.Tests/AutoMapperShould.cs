using AutoMapper;
using NUnit.Framework;
using PoC.Services.Startup;

namespace Services.Tests
{
    public class AutoMapperShould
    {

        [Test]
        public void HaveValidConfiguration()
        {
            var configuration = new AutoMapper.MapperConfiguration((System.Action<IMapperConfigurationExpression>)(cfg => cfg.ConfigureMapper()));

            configuration.AssertConfigurationIsValid();
        }
    }
}