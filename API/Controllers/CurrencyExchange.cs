﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PoC.Services.Abstraction;
using PoC.Services.Abstraction.Dto;
using PoC.Repositories.Exceptions;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CurrencyExchange : ControllerBase
    {
        private readonly ICurrencyExchangeService currencyExchangeService;

        public CurrencyExchange(ICurrencyExchangeService currencyExchangeService)
        {
            this.currencyExchangeService = currencyExchangeService;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Exchange(CurrencyExchangeRequest currencyExchangeRequest)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    return Ok(await currencyExchangeService.ExchangeCurrencyAsync(currencyExchangeRequest));
                }
                catch (NotFoundException)
                {
                    return NotFound();
                }
            }
            else
            {
                return BadRequest();
            }
        }

    }
}
