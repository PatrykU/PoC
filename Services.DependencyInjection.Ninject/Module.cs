﻿using AutoMapper;
using FluentValidation;
using Ninject.Activation;
using Ninject.Modules;
using PoC.Services.Abstraction;
using PoC.Services.Startup;

namespace PoC.Services.DependencyInjection.Ninject
{
    public class Module : NinjectModule
    {
        public override void Load()
        {
            Bind<IMapper>().ToMethod(CreateMapper).InSingletonScope();

            Bind<ICurrencyExchangeService>().To<CurrencyExchangeService>();

            AssemblyScanner.FindValidatorsInAssemblyContaining<PoC.Services.Validators.CurrencyExchangeValidator>()
                .ForEach(match => Bind(match.InterfaceType).To(match.ValidatorType).InThreadScope());
        }

        private IMapper CreateMapper(IContext context)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.ConfigureMapper();
            });

            return new Mapper(config);
        }
    }
}
